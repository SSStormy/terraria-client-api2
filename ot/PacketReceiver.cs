﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace OpenTerraria.ot
{
    public class PacketReceivedEventArgs : EventArgs
    {
        public TPacket Packet { get; }

        public PacketReceivedEventArgs(TPacket packet)
        {
            Packet = packet;
        }
    }

    public class PacketReceiver
    {
        public const int BufferSize = 0x1FFFE;

        public TerrariaClient Client { get; }

        private readonly byte[] _packetBuffer = new byte[BufferSize];
        private readonly byte[] _receiveBuffer = new byte[BufferSize];
        private readonly object _receiveLock = new object();
        private bool _isReceiving;
        private int _pBufferEnd;

        private bool _stopFlag;

        public PacketReceiver(TerrariaClient client)
        {
            Client = client;
        }

        public event EventHandler<PacketReceivedEventArgs> PacketReceived = delegate { };

        public void Start()
        {
            if (_isReceiving)
                throw new InvalidOperationException("Already receiving packets! Use .Stop() to cancel");

            Stop();
            _stopFlag = false;
            _isReceiving = true;
            StartReceive();
        }

        public void Stop()
        {
            _isReceiving = false;
            _stopFlag = true;
        }

        internal void Reset()
        {
            // clear all subscriptions
            PacketReceived = delegate { };
        }

        // todo : this sometimes throws on disconnect
        private void StartReceive()
        {
            Client.NetSocket.BeginReceive(_receiveBuffer, 0, BufferSize, SocketFlags.None, EndReceive, null);
        }


        private void EndReceive(IAsyncResult result)
        {
            lock (_receiveLock)
            {
                var bytesReceived = Client.NetSocket.EndReceive(result);

                // todo : move parsing to TPacket?
                // Parsing depends on the implementation of TPacket
                Buffer.BlockCopy(_receiveBuffer, 0, _packetBuffer, _pBufferEnd, bytesReceived);
                _pBufferEnd += bytesReceived;

                if (bytesReceived <= 0)
                    goto end;

                var pBufferPos = 0;
                var bytesLeft = _pBufferEnd;

                while (bytesLeft >= 2)
                {
                    int pLength = BitConverter.ToUInt16(_packetBuffer, pBufferPos);
                    if (bytesLeft < pLength)
                        break;

                    TPacket packet = null;

                    try
                    {
                        packet = TPacket.Build(_packetBuffer, pBufferPos);
                    }
                    catch (Exception ex)
                    {
                        // todo : handle failiure to parse packets
                        Debug.Assert(false);
                    }

                    Task.Run(() =>
                    {
#if RELEASE
                        try
                        {
                            PacketReceived(this, new PacketReceivedEventArgs(packet));
                        }
                        catch (Exception ex)
                        {
                            // todo : properly log this shit
                            Console.WriteLine($"caught {ex}");
                        }
#else
#if DEBUG
                        PacketReceived(this, new PacketReceivedEventArgs(packet));
#endif
#endif
                    });

                    bytesLeft -= pLength;
                    pBufferPos += pLength;
                }

                if (bytesLeft != _pBufferEnd)
                {
                    for (var j = 0; j < bytesLeft; j++)
                        _packetBuffer[j] = _packetBuffer[j + pBufferPos];

                    _pBufferEnd = bytesLeft;
                }
            }

            end:
            if (!_stopFlag)
                StartReceive();
        }

        private void OnPacketReceived(TPacket packet)
        {

        }
    }
}