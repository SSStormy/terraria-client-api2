﻿using System.Net.Sockets;

namespace OpenTerraria.ot
{
    public class PacketSender
    {
        public TerrariaClient Client { get; }

        private readonly object _sendLock = new object();

        public PacketSender(TerrariaClient client)
        {
            Client = client;
        }

        public void Send(TPacket packet)
        {
            lock (_sendLock)
            {
                Client.NetSocket.BeginSend(packet.Raw, 0, packet.RawLength, SocketFlags.None,
                    r => { Client.NetSocket.EndSend(r); }, null);
            }
        }
    }
}