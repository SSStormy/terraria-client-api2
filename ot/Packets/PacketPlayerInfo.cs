﻿namespace OpenTerraria.ot.Packets
{
    public enum PlayerDifficulty
    {
        Normal = 0,
        Mediumcore = 1,
        Hardcore = 2
    }

    public interface IPacketPlayerInfo : IPacketPayloadWrapper
    {
        byte PlayerId { get; set; }
        byte SkinVariant { get; set; }
        byte Hair { get; set; }
        string Name { get; set; }
        byte HairDye { get; set; }
        byte RawHideVisuals1 { get; set; }
        byte RawHideVisuals2 { get; set; }
        byte HideMisc { get; set; }
        TColor HairColor { get; set; }
        TColor SkinColor { get; set; }
        TColor EyeColor { get; set; }
        TColor ShirtColor { get; set; }
        TColor UnderShirtColor { get; set; }
        TColor ShoeColor { get; set; }
        TColor PantsColor { get; set; }
        byte RawDifficulty { get; set; }
    }
}