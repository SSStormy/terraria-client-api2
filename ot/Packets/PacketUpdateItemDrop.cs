﻿using OpenTerraria.ot.Models;

namespace OpenTerraria.ot.Packets
{
    public class PacketUpdateItemDrop : PacketPayloadWrapper
    {
        public const short NewItemUniqueId = 400;

        public short UniqueItemId { get; set; }
        public TVector Position { get; set; }
        public TVector Velocity { get; set; }
        public short Amount { get; set; }
        public byte Prefix { get; set; }
        public byte NoDelay { get; set; }
        public short ItemId { get; set; }

        public PacketUpdateItemDrop(TPacket packet)
        {
            FromPayload(packet.Payload);
        }
    }
}
