﻿using System.IO;
using OpenTerraria.ot.Models;

namespace OpenTerraria.ot.Packets
{
    public class PacketUpdatePlayer : IPacketPayloadWrapper
    {
        public byte PlayerId { get; set; }
        public byte Control { get; set; }
        public byte Pulley { get; set; }
        public byte SelectedItem { get; set; }
        public TVector Position { get; set; }
        // isn't sent over the wire if it's 0
        public TVector Velocity { get; set; }

        public PacketUpdatePlayer(TPacket packet)
        {
            FromPayload(packet.Payload);
        }

        public PacketUpdatePlayer()
        {
        }

        public PayloadLengthTuple BuildPayload()
        {
            using (var pStream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(pStream))
                {
                    writer.Write(PlayerId);
                    writer.Write(Control);
                    writer.Write(Pulley);
                    writer.Write(SelectedItem);
                    writer.Write(Position.X);
                    writer.Write(Position.Y);
                    if(Utils.GetBit(Pulley, 3))
                    {
                        writer.Write(Velocity.X);
                        writer.Write(Velocity.Y);
                    }
                }
                return new PayloadLengthTuple((ushort) pStream.Length, pStream.ToArray());
            }
        }

        public void FromPayload(byte[] payload)
        {
            using (var pStream = new MemoryStream(payload))
            using (var reader = new BinaryReader(pStream))
            {
                PlayerId = reader.ReadByte();
                Control = reader.ReadByte();
                Pulley = reader.ReadByte();
                SelectedItem = reader.ReadByte();
                Position = new TVector(reader.ReadSingle(), reader.ReadSingle());

                if (pStream.Position < pStream.Length)
                    Position = new TVector(reader.ReadSingle(), reader.ReadSingle());
            }
        }
    }
}