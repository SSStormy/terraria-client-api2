﻿namespace OpenTerraria.ot.Packets
{
    public class PacketKillPlayer : PacketPayloadWrapper
    {
        public byte PlayerId { get; set; }
        public byte HitDirection { get; set; }
        public short Damage { get; set; }
        public bool IsPvp { get; set; }
        public string Message { get; set; }

        public PacketKillPlayer(byte playerId, byte hitDirection, short damage, bool isPvp, string message)
        {
            PlayerId = playerId;
            HitDirection = hitDirection;
            Damage = damage;
            IsPvp = isPvp;
            Message = message;
        }
    }
}