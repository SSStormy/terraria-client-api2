﻿namespace OpenTerraria.ot.Packets
{
    // todo : meta-program the payload wrappers?
    public abstract class PacketPayloadWrapBase : IPacketPayloadWrapper
    {
        public PacketPayloadWrapBase()
        {
        }

        public PacketPayloadWrapBase(TPacket packet)
        {
            FromPayload(packet.Payload);
        }

        public PayloadLengthTuple BuildPayload()
            => PayloadIO.GetBytes(this);

        public void FromPayload(byte[] payload)
            => PayloadIO.WriteInto(payload, this);
    }

    public class PacketGeneric<T1> : PacketPayloadWrapBase
    {
        public T1 Value { get; set; }

        public PacketGeneric(T1 val)
        {
            Value = val;
        }

        public PacketGeneric(TPacket packet) : base(packet)
        {
        }
    }

    public class PacketGeneric<T1, T2> : PacketPayloadWrapBase
    {
        public T1 Value1 { get; set; }
        public T2 Value2 { get; set; }

        public PacketGeneric(T1 val1, T2 val2)
        {
            Value1 = val1;
            Value2 = val2;
        }

        public PacketGeneric(TPacket packet) : base(packet)
        {
        }
    }

    public class PacketGeneric<T1, T2, T3> : PacketPayloadWrapBase
    {
        public T1 Value1 { get; set; }
        public T2 Value2 { get; set; }
        public T3 Value3 { get; set; }

        public PacketGeneric(T1 val1, T2 val2, T3 val3)
        {
            Value1 = val1;
            Value2 = val2;
            Value3 = val3;
        }

        public PacketGeneric(TPacket packet) : base(packet)
        {
        }
    }

    public static class PacketGeneric
    {
        public static PacketGeneric<T1> Create<T1>(T1 val)
            => new PacketGeneric<T1>(val);

        public static PacketGeneric<T1, T2> Create<T1, T2>(T1 val1, T2 val2)
            => new PacketGeneric<T1, T2>(val1, val2);

        public static PacketGeneric<T1, T2, T3> Create<T1, T2, T3>(T1 val1, T2 val2, T3 val3)
            => new PacketGeneric<T1, T2, T3>(val1, val2, val3);
    }
}