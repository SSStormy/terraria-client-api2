﻿using System;
using System.Linq;
using OpenTerraria.ot.Models;

namespace OpenTerraria.ot.Packets
{
    public class PacketUpdatePlayerBuff : IPacketPayloadWrapper
    {
        public byte PlayerId { get; set; }
        public byte[] Buffs { get; set; }

        public PacketUpdatePlayerBuff(TPacket packet)
        {
            
        }

        public PacketUpdatePlayerBuff(PlayerBuffs buffs)
        {
            PlayerId = buffs.Parent.PlayerId;
            Buffs = buffs.All.Select(b => b.BuffId).Cast<byte>().ToArray();
        }

        public PayloadLengthTuple BuildPayload()
        {
            var payload = new byte[PlayerBuffs.Max + 1];
            payload[0] = PlayerId;
            Array.Copy(Buffs, 0, payload, 1, PlayerBuffs.Max);
            return new PayloadLengthTuple(PlayerBuffs.Max + 1, payload);
        }

        public void FromPayload(byte[] payload)
            => Buffer.BlockCopy(payload, 0, Buffs, 0, PlayerBuffs.Max);
    }
}
