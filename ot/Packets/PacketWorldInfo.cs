﻿namespace OpenTerraria.ot.Packets
{
    public sealed class PacketWorldInfo : PacketPayloadWrapper
    {
        public int Time { get; set; }
        public byte DayMoonInfo { get; set; }
        public byte MoonPhase { get; set; }
        public short MaxTilesX { get; set; }
        public short MaxTilesY { get; set; }
        public short SpawnX { get; set; }
        public short SpawnY { get; set; }
        public short WorldSurface { get; set; }
        public short RockLayer { get; set; }
        public int WorldId { get; set; }
        public string WorldName { get; set; }
        public byte MoonType { get; set; }
        public byte TreeBackground { get; set; }
        public byte CorruptionBackground { get; set; }
        public byte JungleBackground { get; set; }
        public byte SnowBackground { get; set; }
        public byte HallowBackground { get; set; }
        public byte CrimsonBackground { get; set; }
        public byte DesertBackground { get; set; }
        public byte OceanBackground { get; set; }
        public byte IceBackStyle { get; set; }
        public byte JungleBackStyle { get; set; }
        public byte HellBackStyle { get; set; }
        public float WindSpeedSet { get; set; }
        public byte CloudNumber { get; set; }
        public int Tree1 { get; set; }
        public int Tree2 { get; set; }
        public int Tree3 { get; set; }
        public byte TreeStyle1 { get; set; }
        public byte TreeStyle2 { get; set; }
        public byte TreeStyle3 { get; set; }
        public byte TreeStyle4 { get; set; }
        public int CaveBack1 { get; set; }
        public int CaveBack2 { get; set; }
        public int CaveBack3 { get; set; }
        public byte CaveBackStyle1 { get; set; }
        public byte CaveBackStyle2 { get; set; }
        public byte CaveBackStyle3 { get; set; }
        public byte CaveBackStyle4 { get; set; }
        public float Rain { get; set; }
        public byte EventInfo1 { get; set; }
        public byte EventInfo2{ get; set; }
        public byte EventInfo3 { get; set; }
        public byte EventInfo4 { get; set; }
        public sbyte InvasionType { get; set; }
        public ulong LobbyId { get; set; }

        public PacketWorldInfo(TPacket packet)
        {
            FromPayload(packet.Payload);
        }
    }
}