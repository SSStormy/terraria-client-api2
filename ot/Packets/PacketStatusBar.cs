﻿namespace OpenTerraria.ot.Packets
{
    public class StatusBar : PacketPayloadWrapper
    {
        /// <summary>
        ///     Returns the amount of status bars that have been sent to you.
        /// </summary>
        public int Count { get; internal set; }

        public string Message { get; internal set; }

        public StatusBar(TPacket packet)
        {
            FromPayload(packet.Payload);
        }
    }
}