﻿using OpenTerraria.ot.Models;
using OpenTerraria.ot.Models.Ids;

namespace OpenTerraria.ot.Packets
{
    public class PacketPlayerInventorySlotInfo : PacketPayloadWrapper
    {
        public byte PlayerId { get; set; }
        public byte Slot { get; set; }
        public short Amount { get; set; }
        public byte PrefixId { get; set; }
        public short ItemId { get; set; }

        public PacketPlayerInventorySlotInfo(TPacket packet) : base(packet) 
        {
            
        }

        public PacketPlayerInventorySlotInfo(byte pid, byte slot, TItem item)
        {
            PlayerId = pid;
            Slot = slot;
            Amount = item.Amount;
            PrefixId = (byte)item.Prefix;
            ItemId = (short)item.ItemId;
        }

        public PacketPlayerInventorySlotInfo()
        {
            
        }
    }
}