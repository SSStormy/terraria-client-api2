﻿using System;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot
{
    public partial class TerrariaClient
    {
        public event EventHandler<StatusBarReceivedEventArgs> StatusBarReceived = delegate { };

        public event EventHandler<FatalErrorEventArgs> FatalError = delegate { };

        // todo : event guidelines tell us that we need to wrap event calls in a try catch, should we?
        protected virtual void OnStatsBarReceived(StatusBarReceivedEventArgs e)
            => StatusBarReceived(this, e);

        protected virtual void OnFatalError(FatalErrorEventArgs e)
            => FatalError(this, e);
    }

    public class StatusBarReceivedEventArgs : EventArgs
    {
		public StatusBar Status { get; }

        internal StatusBarReceivedEventArgs(StatusBar status)
        {
            Status = status;
        }
    }

    public class FatalErrorEventArgs : EventArgs
    {
        public string Message { get; }

        internal FatalErrorEventArgs(string message)
        {
            Message = message;
        }
    }
}
