﻿using System;
using System.Collections;

namespace OpenTerraria.ot
{
    public static class Utils
    {
        /// <summary>
        /// Sets the bit specified by the index in the given value by the given boolean bitval.
        /// </summary>
        /// <returns>The new value with the changed bit.</returns>
        public static int SetBit(int value, int index, bool bitval)
        {
            return (value & ~(1 << index)) | (bitval ? 1 : 0 << index);
        }

        /// <summary>
        /// Returns whether bit at the given index is a 1 or a 0.
        /// </summary>
        public static bool GetBit(int value, int index)
        {
            return (value & (1 << index)) != 0;
        }


        // http://stackoverflow.com/questions/560123/convert-from-bitarray-to-byte
        public static byte ConvertToByte(BitArray bits, bool mustBeEightBits = true)
        {
            if (mustBeEightBits)
                if (bits.Count != 8)
                    throw new ArgumentException("bits");

            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);

            return bytes[0];
        }
    }
}
