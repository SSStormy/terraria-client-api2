﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTerraria.ot.Models;

namespace OpenTerraria.ot
{
    public static class GenericTypeConverter
    {
        private class Converter
        {
            public Converter(Action<BinaryWriter, object> writer, Func<BinaryReader, object> reader)
            {
                Write = writer;
                Read = reader;
            }

            public Action<BinaryWriter, object> Write { get; }
            public Func<BinaryReader, object> Read { get; }
        }

        // todo : support arrays, collections
        /// <summary>
        ///     Defines how we should read or write certain types into bytes.
        /// </summary>
        private static readonly Dictionary<Type, Converter> typeConverters = new Dictionary
            <Type, Converter>
        {
            {typeof(bool), new Converter((w, v) => w.Write((bool) v), r => r.ReadBoolean())},
            {typeof(byte), new Converter((w, v) => w.Write((byte) v), r => r.ReadByte())},
            {typeof(char), new Converter((w, v) => w.Write((char) v), r => r.ReadChar())},
            {typeof(decimal), new Converter((w, v) => w.Write((decimal) v), r => r.ReadDecimal())},
            {typeof(double), new Converter((w, v) => w.Write((double) v), r => r.ReadDouble())},
            {typeof(float), new Converter((w, v) => w.Write((float) v), r => r.ReadSingle())},
            {typeof(int), new Converter((w, v) => w.Write((int) v), r => r.ReadInt32())},
            {typeof(long), new Converter((w, v) => w.Write((long) v), r => r.ReadInt64())},
            {typeof(sbyte), new Converter((w, v) => w.Write((sbyte) v), r => r.ReadSByte())},
            {typeof(short), new Converter((w, v) => w.Write((short) v), r => r.ReadInt16())},
            {typeof(uint), new Converter((w, v) => w.Write((uint) v), r => r.ReadUInt32())},
            {typeof(ulong), new Converter((w, v) => w.Write((ulong) v), r => r.ReadUInt64())},
            {typeof(ushort), new Converter((w, v) => w.Write((ushort) v), r => r.ReadUInt16())},
            {typeof(string), new Converter((w, v) => w.Write((string) v), r => r.ReadString())},
            {typeof(TVector), new Converter((w, v) =>
            {
                TVector vec = (TVector)v;
                w.Write(vec.X);
                w.Write(vec.Y);
            }, r => new TVector(r.ReadSingle(), r.ReadSingle()))},
            {typeof(TColor), new Converter(
            (w, v) =>
            {
                TColor color = (TColor) v;
                w.Write(color.Red);
                w.Write(color.Green);
                w.Write(color.Blue);
            }, r =>
            {
                TColor color = new TColor
                {
                    Red = r.ReadByte(),
                    Green = r.ReadByte(),
                    Blue = r.ReadByte()
                };
                return color;
            })}
        };

        public static bool CanConvert(Type type)
            => typeConverters.ContainsKey(type);

        public static void Write(BinaryWriter writer, object obj)
        {
            typeConverters[obj.GetType()].Write(writer, obj);
        }

        public static object Read(BinaryReader reader, Type type)
        {
            if (!CanConvert(type))
                throw new InvalidOperationException($"Cannot read type {type.Name}");

            return typeConverters[type].Read(reader);
        }
    }
}