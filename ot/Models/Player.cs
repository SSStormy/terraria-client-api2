﻿using System.Collections;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot.Models
{
    public class Player
    {
        public enum TeamType : byte
        {
            None = 0,
            Red = 1,
            Green = 2,
            Blue = 3,
            Yellow = 4,
            Pink
        }

        public const int DefaultHp = 100;
        public const int DefaultMana = 10;

        public byte PlayerId { get; internal set; }

        public bool IsActive { get; internal set; }

        /// <summary>
        /// Returns whether this player is the player we are controling
        /// </summary>
        public bool IsUs { get; internal set; }

        /// <summary>Gets whether this player is the server represented as a player.</summary>
        public bool IsServer => PlayerId == byte.MaxValue;

        public PlayerInventory Inventory { get; internal set; }
        public PlayerBuffs Buffs { get; internal set; }
        public PlayerInfo LoadoutInfo { get; internal set; }

        public bool IsPvp { get; internal set; }
        public short Health { get; internal set; }
        public short HealthMax { get; internal set; }

        public short Mana { get; internal set; }
        public short ManaMax { get; internal set; }

        public TeamType Team { get; internal set; }

        public TVector Position { get; internal set; } // set?
        public TVector Velocity { get; internal set; } // set?

        public byte SelectedItem { get; internal set; } // todo : set selected item

        public bool IsGoingUp { get; internal set; }
        public bool IsGoingDown { get; internal set; }
        public bool IsGoingLeft { get; internal set; }
        public bool IsGoingRight { get; internal set; }
        public bool IsJumping { get; internal set; }
        public bool IsUsingItem { get; internal set; }

        /// <summary>Represents a east or west direction.</summary>
        public bool Direction { get; internal set; } // set;

        public byte PulleyFlags { get; internal set; }

        public TerrariaClient Client { get; }

        // todo : set this true when we receive a kill player packet (0x2C)
        public bool IsDead { get; internal set; }

        internal Player(TerrariaClient client, bool isStub = false)
        {
            Client = client;
            if (!isStub) return;

            Inventory = new PlayerInventory(this);
            Buffs = new PlayerBuffs(this);
            LoadoutInfo = new PlayerInfo(this);
        }

        internal void Update(PacketUpdatePlayer update)
        {
            BitArray control = new BitArray(new[] { update.Control });
            IsGoingUp = control[0];
            IsGoingDown = control[1];
            IsGoingLeft = control[2];
            IsGoingRight = control[3];
            IsJumping = control[4];
            IsUsingItem = control[5];
            Direction = control[6];

            // todo : parse pulley flags
            PulleyFlags = update.Pulley;
            SelectedItem = update.SelectedItem;

            Position = update.Position;
            Velocity = update.Velocity;
        }
    }

    public sealed class CurrentPlayer : Player
    {
        internal CurrentPlayer(TerrariaClient client, PlayerInfo playerInfo,
            PlayerInventory inventory, PlayerBuffs buffs) : this(client)
        {
            playerInfo = playerInfo == null ? PlayerInfo.Default : (PlayerInfo)playerInfo.Clone();
            inventory = inventory ?? new PlayerInventory(this);
            buffs = buffs ?? new PlayerBuffs(this);

            LoadoutInfo = playerInfo;
            Inventory = new CurrentPlayerInventory(inventory);
            Buffs = new CurrentPlayerBuffs(buffs);

            if (LoadoutInfo.Parent == null) LoadoutInfo.Parent = this;
            if (Inventory.Parent == null) Inventory.Parent = this;
            if (Buffs.Parent == null) Buffs.Parent = this;

            LoadoutInfo.IsStub = false;
            Inventory.IsStub = false;
            Buffs.IsStub = false;
        }

        // todo : send update packet if any of the vars in it have been modified.
        public PacketUpdatePlayer GetUpdatePacket()
        {
            var ret = new PacketUpdatePlayer
            {
                PlayerId = PlayerId,
                Control =
                    Utils.ConvertToByte(new BitArray(new[]
                    {
                        IsGoingUp, IsGoingDown,
                        IsGoingLeft, IsGoingRight, IsJumping,
                        IsUsingItem, Direction
                    }), false),
                Pulley = PulleyFlags,
                SelectedItem = SelectedItem,
                Position = Position,
                Velocity = Velocity
            };

            return ret;
        }

        internal CurrentPlayer(Player player) : this(player.Client)
        {
            Inventory = new CurrentPlayerInventory(player.Inventory);
        }

        private CurrentPlayer(TerrariaClient client) : base(client)
        {
            IsUs = true;
        }

        public void SetHealth(short current, short max)
        {
            Client.Sender.Send(TPacket.Build(TPacketType.PlayerHealth, PacketGeneric.Create(PlayerId, current, max)));
            Health = current;
            HealthMax = max;
        }

        public void SetMana(short current, short max)
        {
            Client.Sender.Send(TPacket.Build(TPacketType.PlayerMana, PacketGeneric.Create(PlayerId, current, max)));
            Mana = current;
            ManaMax = max;
        }

        public void SetTeam(TeamType team)
        {
            Client.Sender.Send(TPacket.Build(TPacketType.PlayerTeam, PacketGeneric.Create(PlayerId, (byte)team)));
            Team = team;
        }

        public void TogglePvp(bool? state)
        {
            // guarantee that state will not be null via recursion.
            if (state == null)
                TogglePvp(!IsPvp);

            // ReSharper disable once PossibleInvalidOperationException
            Client.Sender.Send(TPacket.Build(TPacketType.TogglePvp, PacketGeneric.Create(PlayerId, state.Value)));
            IsPvp = state.Value;
        }

        public void KillMe(string message = " died.", short damage = 0, bool isPvp = false, byte hitDir = 0)
        {
            damage = damage == 0 ? (short)HealthMax : damage;
            var packet = new PacketKillPlayer(PlayerId, hitDir, damage, isPvp, message);
            Client.Sender.Send(TPacket.Build(TPacketType.KillPlayer, packet));
            IsDead = true;
        }

        public void Respawn(short x, short y)
        {
            if (!IsDead)
                return;

            IsDead = false;
            Client.Sender.Send(TPacket.Build(TPacketType.SpawnPlayer, PacketGeneric.Create(PlayerId, x, y)));
        }
    }
}