﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenTerraria.ot.Models.Ids;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot.Models
{
    public struct TBuff
    {
        public TBuffId BuffId { get; set; }
        public ushort Time { get; set; }

        public TBuff(TBuffId buffId, ushort time = PlayerBuffs.DefaultTime)
        {
            BuffId = buffId;
            Time = time;
        }

        public static implicit operator TBuff(TBuffId id)
            => new TBuff(id);
    }

    public class PlayerBuffs : PlayerChildObject, IEnumerable
    {
        public const int DefaultTime = 3600;

        // todo : get/set buff time?
        public const int Max = 22;

        public IEnumerable<TBuff> All => Array.AsReadOnly(InternalBuffs);

        public TBuff this[int i] => InternalBuffs[i];

        internal TBuff[] InternalBuffs = new TBuff[Max];
        private int _curAddBuffIndex;

        public PlayerBuffs()
        {
        }

        internal PlayerBuffs(Player parent) : base(parent)
        {
        }

        internal void Update(PacketUpdatePlayerBuff update)
        {
            InternalBuffs = update.Buffs.Select(b => new TBuff((TBuffId)b)).ToArray();
        }

        IEnumerator IEnumerable.GetEnumerator()
            => All.GetEnumerator();

        public virtual void SetBuff(int index, TBuff buff)
        {
            ThrowIfOutOfBounds(index);

            if (IsStub)
                InternalBuffs[index] = buff;
            else
                throw new NotSupportedException("Inventory values can only be set for the current player.");
        }

        public void AddBuff(TBuff buff)
        {
            if (_curAddBuffIndex > Max)
                _curAddBuffIndex = 0;

            SetBuff(_curAddBuffIndex++, buff);
        }

        protected void ThrowIfOutOfBounds(int slot)
        {
            if (slot > Max || 0 > slot)
                throw new IndexOutOfRangeException(
                    $"The slot argument passed to SetItem is out of bounds: {slot}, Min: 0 Max: {Max}");
        }

        public IEnumerator GetEnumerator()
            => All.GetEnumerator();
    }

    public sealed class CurrentPlayerBuffs : PlayerBuffs
    {
        public CurrentPlayerBuffs(PlayerBuffs buffs) : base(buffs.Parent)
        {
            buffs.InternalBuffs.CopyTo(InternalBuffs, 0);
        }

        // 55 sends both the buff and the duration over the wire
        // should we always send a constant number?
        // mount buffs stay active for 3600 seconds
        // what if we create a new thread (stop it in Disconnect) that has a timer, removes buffs when needed?
        // todo : buff times
        public override void SetBuff(int index, TBuff buff)
        {
            ThrowIfOutOfBounds(index);
            InternalBuffs[index] = buff;
            Parent.Client.Sender.Send(
                TPacket.Build(TPacketType.AddPlayerBuff,
                PacketGeneric.Create(Parent.PlayerId, buff.BuffId, buff.Time)));
        }
    }
}