﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot.Models
{
    public enum MoonPhaseType
    {
        FullMoon = 0,
        WaningGibbous = 1,
        ThirdQuarter = 2,
        WaningCrescent = 3,
        NewMoon = 4,
        WaxingCrescent = 5,
        FirstQuarter = 6,
        WaxingGibbous = 7
    }

    public class TWorld
    {
        public const int MaxWorldItems = 401;
        public const int MaxKillCount = 540;
        public const int MaxPlayers = byte.MaxValue + 1;

        public int RawTime
        {
            get { return _time; }
            internal set
            {
                _time = value;

                // please call an ambulance
                if (!IsDay)
                    value += 54000;

                var time = value/86400.0*24.0 - 7.5 - 12.0;

                if (time < 0.0)
                    time += 24.0;

                var hour = (int) time;
                var minute = (int) ((time - hour)*60.0);

                Time = new TimeSpan(0, hour, minute);
            }
        }

        public TimeSpan Time { get; private set; }
        public MoonPhaseType MoonPhase { get; private set; }
        public short MaxTilesX { get; private set; }
        public short MaxTilesY { get; private set; }
        public short SpawnX { get; private set; }
        public short SpawnY { get; private set; }
        public short WorldSurface { get; private set; }
        public short RockLayer { get; private set; }
        public int WorldId { get; private set; }
        public string WorldName { get; private set; }
        // todo : moon type enum
        public byte MoonType { get; private set; }
        public byte TreeBackground { get; private set; }
        public byte CorruptionBackground { get; private set; }
        public byte JungleBackground { get; private set; }
        public byte SnowBackground { get; private set; }
        public byte HallowBackground { get; private set; }
        public byte CrimsonBackground { get; private set; }
        public byte DesertBackground { get; private set; }
        public byte OceanBackground { get; private set; }
        public byte IceBackStyle { get; private set; }
        public byte JungleBackStyle { get; private set; }
        public byte HellBackStyle { get; private set; }
        public float WindSpeedSet { get; private set; }
        public byte CloudNumber { get; private set; }
        public int Tree1 { get; private set; }
        public int Tree2 { get; private set; }
        public int Tree3 { get; private set; }
        public byte TreeStyle1 { get; private set; }
        public byte TreeStyle2 { get; private set; }
        public byte TreeStyle3 { get; private set; }
        public byte TreeStyle4 { get; private set; }
        public int CaveBack1 { get; private set; }
        public int CaveBack2 { get; private set; }
        public int CaveBack3 { get; private set; }
        public byte CaveBackStyle1 { get; private set; }
        public byte CaveBackStyle2 { get; private set; }
        public byte CaveBackStyle3 { get; private set; }
        public byte CaveBackStyle4 { get; private set; }
        public float Rain { get; private set; }
        // todo : invasion type enum
        public sbyte InvasionType { get; private set; }
        public ulong LobbyId { get; private set; }

        // todo : parse WorldTime packets
        public short SunModY { get; internal set; }
        public short MoonModY { get; internal set; }

        public bool IsDay { get; internal set; }
        public bool IsBloodmoon { get; internal set; }
        public bool IsEclipse { get; internal set; }

        public bool IsShadowOrbSmashed { get; internal set; }
        public bool IsEyeOfCthuluDefeated { get; internal set; }
        public bool IsEowOrBrainDefeated { get; internal set; }
        public bool IsSkeletronDefeated { get; internal set; }
        public bool IsHardmode { get; internal set; }
        public bool IsClownDefeated { get; internal set; }
        public bool IsPlanteraDefeated { get; internal set; }

        public bool IsDestroyedDefeated { get; internal set; }
        public bool AreTwinsDefeated { get; internal set; }
        public bool IsSkeletronPrimeDefeated { get; internal set; }
        public bool IsAnyMechBossDefeated { get; internal set; }
        public bool IsCrimson { get; internal set; }
        public bool IsPumpkinMoonActive { get; internal set; }
        public bool IsSnowMoonActive { get; internal set; }

        public bool IsExpertMode { get; internal set; }
        public bool IsFastForwardingTime { get; internal set; }
        public bool IsSlimeRainActive { get; internal set; }
        public bool IsSlimeKingDefeated { get; internal set; }
        public bool IsQueenBeeDefeated { get; internal set; }
        public bool IsFishronDefeated { get; internal set; }
        public bool AreMartiansDefeated { get; internal set; }
        public bool AreCultistsDefeated { get; internal set; }

        public bool IsMoonlordDefeated { get; internal set; }
        public bool IsHalloweenKingDefeated { get; internal set; }
        public bool IsHalloweenTreeDefeated { get; internal set; }
        public bool IsChristmasIceQueenDefeated { get; internal set; }
        public bool IsChristmasSantankDefeated { get; internal set; }
        public bool IsChristmasTreeDefeated { get; internal set; }
        public bool IsGolemDefeated { get; internal set; }

        public bool IsCloudBgActive { get; internal set; }

        public IReadOnlyCollection<int> KillCount => Array.AsReadOnly(InternalKillCount);
        public IReadOnlyCollection<TWorldItem> WorldItems => Array.AsReadOnly(InternalWorldItems.Where(i => i.IsActive).ToArray());

        private int _time;

        internal int[] InternalKillCount = new int[MaxKillCount];
        internal TWorldItem[] InternalWorldItems = new TWorldItem[MaxWorldItems];
        internal Player[] InternalPlayers = new Player[MaxPlayers];
        // todo : add player to InternalPlayerList

        public TerrariaClient Client { get; }

        internal TWorld(TerrariaClient client, PacketWorldInfo info)
        {
            Client = client;

            for(int i = 0; i < MaxWorldItems; i++)
                InternalWorldItems[i] = new TWorldItem();

            for (int i = 0; i < MaxPlayers; i++)
                InternalPlayers[i] = new Player(client, true);

            Update(info);
        }

        internal void Update(PacketWorldInfo info)
        {
            var timeInfo = new BitArray(new[] { info.DayMoonInfo });
            IsDay = timeInfo[0];
            IsBloodmoon = timeInfo[1];
            IsEclipse = timeInfo[2];
            RawTime = info.Time;

            MoonPhase = (MoonPhaseType)info.MoonPhase;
            MaxTilesX = info.MaxTilesX;
            MaxTilesY = info.MaxTilesY;
            SpawnX = info.SpawnX;
            SpawnY = info.SpawnY;
            WorldSurface = info.WorldSurface;
            RockLayer = info.RockLayer;
            WorldId = info.WorldId;
            WorldName = info.WorldName;
            MoonType = info.MoonType;
            TreeBackground = info.TreeBackground;
            CorruptionBackground = info.CorruptionBackground;
            JungleBackground = info.JungleBackground;
            SnowBackground = info.SnowBackground;
            HallowBackground = info.HallowBackground;
            CrimsonBackground = info.CrimsonBackground;
            DesertBackground = info.DesertBackground;
            OceanBackground = info.OceanBackground;
            IceBackStyle = info.IceBackStyle;
            JungleBackStyle = info.JungleBackStyle;
            HellBackStyle = info.HellBackStyle;
            WindSpeedSet = info.WindSpeedSet;
            CloudNumber = info.CloudNumber;
            Tree1 = info.Tree1;
            Tree2 = info.Tree2;
            Tree3 = info.Tree3;
            TreeStyle1 = info.TreeStyle1;
            TreeStyle2 = info.TreeStyle2;
            TreeStyle3 = info.TreeStyle3;
            TreeStyle4 = info.TreeStyle4;
            CaveBack1 = info.CaveBack1;
            CaveBack2 = info.CaveBack2;
            CaveBack3 = info.CaveBack3;
            CaveBackStyle1 = info.CaveBackStyle1;
            CaveBackStyle2 = info.CaveBackStyle2;
            CaveBackStyle3 = info.CaveBackStyle3;
            CaveBackStyle4 = info.CaveBackStyle4;
            Rain = info.Rain;
            InvasionType = info.InvasionType;
            LobbyId = info.LobbyId;

            var events1 = new BitArray(new[] { info.EventInfo1 });
            IsShadowOrbSmashed = events1[0];
            IsEyeOfCthuluDefeated = events1[1];
            IsEowOrBrainDefeated = events1[2];
            IsSkeletronDefeated = events1[3];
            IsHardmode = events1[4];
            IsClownDefeated = events1[5];
            // events1[6] is Main.serverSideCharacter.
            IsPlanteraDefeated = events1[7];

            var events2 = new BitArray(new[] { info.EventInfo2 });
            IsDestroyedDefeated = events2[0];
            AreTwinsDefeated = events2[1];
            IsSkeletronPrimeDefeated = events2[2];
            IsAnyMechBossDefeated = events2[3];
            IsCloudBgActive = events2[4];
            IsCrimson = events2[5];
            IsPumpkinMoonActive = events2[6];
            IsSnowMoonActive = events2[7];

            var events3 = new BitArray(new[] { info.EventInfo3 });
            IsExpertMode = events3[0];
            IsFastForwardingTime = events3[1];
            IsSlimeRainActive = events3[2];
            IsSlimeKingDefeated = events3[3];
            IsQueenBeeDefeated = events3[4];
            IsFishronDefeated = events3[5];
            AreMartiansDefeated = events3[6];
            AreCultistsDefeated = events3[7];

            var events4 = new BitArray(new[] { info.EventInfo4 });
            IsMoonlordDefeated = events4[0];
            IsHalloweenKingDefeated = events4[1];
            IsHalloweenTreeDefeated = events4[2];
            IsChristmasIceQueenDefeated = events4[3];
            IsChristmasSantankDefeated = events4[4];
            IsChristmasTreeDefeated = events4[5];
            IsGolemDefeated = events4[6];
        }
    }
}