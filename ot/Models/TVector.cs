﻿namespace OpenTerraria.ot.Models
{
    public struct TVector
    {
        public float X { get; set; }
        public float Y { get; set; }

        public TVector(float x = 0, float y = 0)
        {
            X = x;
            Y = y;
        }
    }
}
