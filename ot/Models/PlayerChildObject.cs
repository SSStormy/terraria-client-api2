﻿namespace OpenTerraria.ot.Models
{
    public class PlayerChildObject
    {
        /// <summary>
        ///     Returns whether this instance is ONLY meant to be used for the Login algorithm
        /// </summary>
        [PayloadIO.IgnoreMember]
        public bool IsStub { get; internal set; }

        [PayloadIO.IgnoreMember]
        public Player Parent { get; internal set; }

        /// <summary>
        /// Creates a new instance for use with the Login alogrithm
        /// </summary>

        public PlayerChildObject()
        {
            IsStub = true;
        }

        internal PlayerChildObject(Player parent)
        {
            Parent = parent;
        }
    }
}
