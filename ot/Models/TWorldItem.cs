﻿namespace OpenTerraria.ot.Models
{
    public class TWorldItem
    {
        public const byte DefaultOwner = byte.MaxValue;

        internal bool IsActive { get; set; } 

        public TItem Item { get; internal set; }
        public TVector Position { get; internal set; }
        public TVector Velocity { get; internal set; }
        public byte NoDelay { get; internal set; }

        // todo : replace owner in TWorldItem when we have player caching
        public byte Owner { get; internal set; } = DefaultOwner;

        internal TWorldItem()
        {
        }
    }
}
