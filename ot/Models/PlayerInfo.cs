﻿using System;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot.Models
{
    /// <summary>
    ///     Doubles as a packet
    /// </summary>
    public class PlayerInfo : PlayerChildObject, ICloneable, IPacketPlayerInfo
    {
        public static class Indexes
        {
            public const int HasExtraAccessoryBit = 2;
            public const int MaxAccessories = 6;
            public const int HasHiddenMinionBit = 0;
            public const int HasHiddenLightMinionBit = 1;
            public const int AccessoryOffset = 2;
        }

        private const string CantChangeValueException =
            "This value cannot be altered on remote players or after logging in.";


        [PayloadIO.IgnoreMemberAttribute]
        public PlayerDifficulty Difficulty
        {
            get
            {
                return _difficulty;
            }
            set
            {
                // todo : test setting difficulty
                ThrowIfNotStub();
                // set bits (0 1 2) to false
                for (var i = 0; i < 3; i++)
                    RawDifficulty = (byte) Utils.SetBit(RawDifficulty, i, false);

                // set the according bit to true.
                RawDifficulty = (byte) Utils.SetBit(RawDifficulty, (int) value, true);
                _difficulty = value;
            }
        }

        // todo : test changing these values after login
        [PayloadIO.IgnoreMemberAttribute]
        public bool HasExtraAccessory
        {
            get { return Utils.GetBit(RawDifficulty, Indexes.HasExtraAccessoryBit); }
            set
            {
                ThrowIfNotStubOrUs();
                RawDifficulty = (byte) Utils.SetBit(RawDifficulty, Indexes.HasExtraAccessoryBit, value);
                SendUpdatePacket();
            }
        }

        // todo : these aren't returning the correct value
        [PayloadIO.IgnoreMemberAttribute]
        public bool IsMinionVisible
        {
            get { return Utils.GetBit(RawHideVisuals2, Indexes.HasHiddenMinionBit); }
            set
            {
                ThrowIfNotStubOrUs();
                RawHideVisuals2 = (byte) Utils.SetBit(RawHideVisuals2, Indexes.HasHiddenMinionBit, value);
                SendUpdatePacket();
            }
        }

        [PayloadIO.IgnoreMemberAttribute]
        public bool IsLightMinionVisible
        {
            get { return Utils.GetBit(RawHideVisuals2, Indexes.HasHiddenLightMinionBit); }
            set
            {
                ThrowIfNotStubOrUs();
                RawHideVisuals2 = (byte) Utils.SetBit(RawHideVisuals2, Indexes.HasHiddenLightMinionBit, value);
                SendUpdatePacket();
            }
        }

        [PayloadIO.IgnoreMemberAttribute]
        public static PlayerInfo Default => new PlayerInfo
        {
            SkinVariant = 0,
            Hair = 0,
            Name = "OpenTerraria",
            HairDye = 0,
            RawHideVisuals1 = 0,
            RawHideVisuals2 = 0,
            HideMisc = 0,
            HairColor = new TColor(0xd7, 0x5a, 0x37),
            SkinColor = new TColor(0xff, 0x7f, 0x5a),
            EyeColor = new TColor(0x69, 0x5a, 0x4b),
            ShirtColor = new TColor(0xaf, 0xa5, 0x8c),
            UnderShirtColor = new TColor(0xa0, 0xb4, 0xd7),
            PantsColor = new TColor(0xff, 0xe6, 0xaf),
            ShoeColor = new TColor(0xa0, 0x69, 0x3c),
            RawDifficulty = 0
        };

        private PlayerDifficulty _difficulty;
        private TColor _eyeColor;
        private byte _hair;
        private TColor _hairColor;
        private byte _hairDye;
        private byte _hideMisc;
        private byte _hideVisuals1;
        private byte _hideVisuals2;
        private string _name;
        private TColor _pantsColor;
        private byte _rawDifficulty;
        private TColor _shirtColor;
        private TColor _shoeColor;
        private TColor _skinColor;
        private byte _skinVariant;
        private TColor _underShirtColor;

        internal PlayerInfo(Player parent) : base(parent)
        {
        }

        private PlayerInfo()
        {
            
        }

        internal PlayerInfo(TPacket packet)
        {
            FromPayload(packet.Payload);
            IsStub = false;

            byte diff = 0;

            if (Utils.GetBit(RawDifficulty, 0))
                ++diff;
            if (Utils.GetBit(RawDifficulty, 1))
                diff += 2;
            if (diff > 2)
                diff = 2;

            _difficulty = (PlayerDifficulty) diff;
        }

        public object Clone()
        {
            var clone = (PlayerInfo) MemberwiseClone();
            clone.Name = string.Copy(Name);
            return clone;
        }

        // only used for packet building
        public byte PlayerId { get; set; }

        public byte SkinVariant
        {
            get { return _skinVariant; }
            set { SetValue(value, ref _skinVariant); }
        }

        public byte Hair
        {
            get { return _hair; }
            set { SetValue(value, ref _hair); }
        }

        public string Name
        {
            get { return _name; }
            set { SetValue(value, ref _name); }
        }

        public byte HairDye
        {
            get { return _hairDye; }
            set { SetValue(value, ref _hairDye); }
        }

        public byte RawHideVisuals1
        {
            get { return _hideVisuals1; }
            set { SetValue(value, ref _hideVisuals1); }
        }

        public byte RawHideVisuals2
        {
            get { return _hideVisuals2; }
            set { SetValue(value, ref _hideVisuals2); }
        }

        public byte HideMisc
        {
            get { return _hideMisc; }
            set { SetValue(value, ref _hideMisc); }
        }

        public TColor HairColor
        {
            get { return _hairColor; }
            set { SetValue(value, ref _hairColor); }
        }

        public TColor SkinColor
        {
            get { return _skinColor; }
            set { SetValue(value, ref _skinColor); }
        }

        public TColor EyeColor
        {
            get { return _eyeColor; }
            set { SetValue(value, ref _eyeColor); }
        }

        public TColor ShirtColor
        {
            get { return _shirtColor; }
            set { SetValue(value, ref _shirtColor); }
        }

        public TColor UnderShirtColor
        {
            get { return _underShirtColor; }
            set { SetValue(value, ref _underShirtColor); }
        }

        public TColor PantsColor
        {
            get { return _pantsColor; }
            set { SetValue(value, ref _pantsColor); }
        }

        public TColor ShoeColor
        {
            get { return _shoeColor; }
            set { SetValue(value, ref _shoeColor); }
        }


        public byte RawDifficulty
        {
            get { return _rawDifficulty; }
            set { SetValue(value, ref _rawDifficulty); }
        }


        public PayloadLengthTuple BuildPayload()
        {
            PlayerId = Parent.PlayerId;
            if (PlayerId == byte.MaxValue)
                throw new InvalidOperationException("Illegal player id!");

            return PayloadIO.GetBytes(this);
        }

        public void FromPayload(byte[] payload)
            => PayloadIO.WriteInto(payload, this);

        public virtual bool IsAccessoryVisibleAt(int index)
        {
            if (index <= 0 || index > Indexes.MaxAccessories)
                throw new IndexOutOfRangeException();

            return Utils.GetBit(RawHideVisuals1, index + Indexes.AccessoryOffset);
        }

        public virtual void SetAccessoryVisibilityAt(int index, bool value)
        {
            ThrowIfNotStubOrUs();
            Utils.SetBit(RawHideVisuals1, index + Indexes.AccessoryOffset, value);
            SendUpdatePacket();
        }

        private void SetValue<T>(T value, ref T set)
        {
            ThrowIfNotStub();
            set = value;
        }

        private void ThrowIfNotStub()
        {
            if (!IsStub)
                throw new NotSupportedException(CantChangeValueException);
        }

        private void ThrowIfNotStubOrUs()
        {
            if (!IsStub || !Parent.IsUs)
                throw new NotSupportedException(CantChangeValueException);
        }

        private void SendUpdatePacket()
            => Parent.Client.Sender.Send(TPacket.Build(TPacketType.PlayerInfo, this));
    }
}