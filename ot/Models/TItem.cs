﻿using OpenTerraria.ot.Models.Ids;

namespace OpenTerraria.ot.Models
{
    public struct TItem
    {
        public TItemId ItemId { get; set; }
        public TItemPrefix Prefix { get; set; }
        public short Amount { get; set; }

        public TItem(TItemId itemId, TItemPrefix prefix = TItemPrefix.None, short amount = 1)
        {
            ItemId = itemId;
            Prefix = prefix;
            Amount = amount;
        }

        public static implicit operator TItem(TItemId id)
            => new TItem(id);
    }
}
