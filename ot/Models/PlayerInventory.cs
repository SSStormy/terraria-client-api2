﻿using System;
using System.Collections;
using System.Collections.Generic;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot.Models
{
    public class PlayerInventory : PlayerChildObject, IEnumerable
    {
        public static class Indexes
        {
            public const int Max = 179;

            public const int Helmet = 59;
            public const int Chestplate = 60;
            public const int Leggings = 61;
            public const int SocialHelmet = 69;
            public const int SocialChestplate = 70;
            public const int SocialLeggings = 71;
            public const int HelmetDye = 79;
            public const int ChestplateDye = 80;
            public const int LeggingDye = 81;
            public const int Pet = 89;
            public const int LightPet = 90;
            public const int Minecart = 91;
            public const int Mount = 92;
            public const int GrapplingHook = 93;
            public const int PetDye = 94;
            public const int LightPetDye = 95;
            public const int MinecartDye = 96;
            public const int MountDye = 97;
            public const int GrapplingHookDye = 98;
            public const int Trash = 179;

            // todo : test these indexes
            public static readonly Tuple<int, int> Hotbar = new Tuple<int, int>(0, 9);
            public static readonly Tuple<int, int> Inventory = new Tuple<int, int>(10, 49);
            public static readonly Tuple<int, int> Coins = new Tuple<int, int>(50, 53);
            public static readonly Tuple<int, int> Ammo = new Tuple<int, int>(54, 57);
            public static readonly Tuple<int, int> Armor = new Tuple<int, int>(59, 61);
            public static readonly Tuple<int, int> Accessories = new Tuple<int, int>(62, 66);
            public static readonly Tuple<int, int> SocialArmor = new Tuple<int, int>(69, 71);
            public static readonly Tuple<int, int> SocialAcesorries = new Tuple<int, int>(72, 76);
            public static readonly Tuple<int, int> ArmorDyes = new Tuple<int, int>(79, 81);
            public static readonly Tuple<int, int> AccesoryDyes = new Tuple<int, int>(82, 86);
            public static readonly Tuple<int, int> MiscEquips = new Tuple<int, int>(89, 89);
            public static readonly Tuple<int, int> MiscDyes = new Tuple<int, int>(94, 98);
            public static readonly Tuple<int, int> Piggybank = new Tuple<int, int>(99, 138);
            public static readonly Tuple<int, int> Safe = new Tuple<int, int>(139, 178);
        }

        public IEnumerable<TItem> All => Array.AsReadOnly(InternalItems);

        // todo : maybe we don't need to create a new array segment every time we want to access one of the inventory partitions?
        // we can't use { get; } = <...>
        public IEnumerable<TItem> Hotbar
            => new ArraySegment<TItem>(InternalItems, Indexes.Hotbar.Item1, Indexes.Hotbar.Item2);

        public IEnumerable<TItem> Inventory
            => new ArraySegment<TItem>(InternalItems, Indexes.Inventory.Item1, Indexes.Inventory.Item2);

        public IEnumerable<TItem> Ammo
            => new ArraySegment<TItem>(InternalItems, Indexes.Ammo.Item1, Indexes.Ammo.Item2);

        public IEnumerable<TItem> Coins
            => new ArraySegment<TItem>(InternalItems, Indexes.Coins.Item1, Indexes.Coins.Item2);

        public IEnumerable<TItem> Armor
            => new ArraySegment<TItem>(InternalItems, Indexes.Armor.Item1, Indexes.Armor.Item2);

        public IEnumerable<TItem> SocialArmor
            => new ArraySegment<TItem>(InternalItems, Indexes.SocialArmor.Item1, Indexes.SocialArmor.Item2);

        public IEnumerable<TItem> Accessories
            => new ArraySegment<TItem>(InternalItems, Indexes.Accessories.Item1, Indexes.Accessories.Item2);

        public IEnumerable<TItem> SocialAcesorries
            => new ArraySegment<TItem>(InternalItems, Indexes.SocialAcesorries.Item1, Indexes.SocialAcesorries.Item2);

        public IEnumerable<TItem> ArmorDyes
            => new ArraySegment<TItem>(InternalItems, Indexes.ArmorDyes.Item1, Indexes.ArmorDyes.Item2);

        public IEnumerable<TItem> AccesoryDyes
            => new ArraySegment<TItem>(InternalItems, Indexes.AccesoryDyes.Item1, Indexes.AccesoryDyes.Item2);

        public IEnumerable<TItem> MiscEquips
            => new ArraySegment<TItem>(InternalItems, Indexes.MiscEquips.Item1, Indexes.MiscEquips.Item2);

        public IEnumerable<TItem> MiscDyes
            => new ArraySegment<TItem>(InternalItems, Indexes.MiscDyes.Item1, Indexes.MiscDyes.Item2);

        public IEnumerable<TItem> PiggyBank
            => new ArraySegment<TItem>(InternalItems, Indexes.Piggybank.Item1, Indexes.Piggybank.Item2);

        public IEnumerable<TItem> Safe
            => new ArraySegment<TItem>(InternalItems, Indexes.Safe.Item1, Indexes.Safe.Item2);

        public virtual TItem Helmet
        {
            get { return InternalItems[Indexes.Helmet]; }
            set { SetItem(Indexes.Helmet, value); }
        }

        public virtual TItem Chestplate
        {
            get { return InternalItems[Indexes.Chestplate]; }
            set { SetItem(Indexes.Chestplate, value); }
        }

        public virtual TItem Leggings
        {
            get { return InternalItems[Indexes.Leggings]; }
            set { SetItem(Indexes.Leggings, value); }
        }

        public virtual TItem SocialHelmet
        {
            get { return InternalItems[Indexes.SocialHelmet]; }
            set { SetItem(Indexes.SocialHelmet, value); }
        }

        public virtual TItem SocialChestplate
        {
            get { return InternalItems[Indexes.SocialChestplate]; }
            set { SetItem(Indexes.SocialChestplate, value); }
        }

        public virtual TItem SocialLeggings
        {
            get { return InternalItems[Indexes.SocialLeggings]; }
            set { SetItem(Indexes.SocialLeggings, value); }
        }

        public virtual TItem HelmetDye
        {
            get { return InternalItems[Indexes.HelmetDye]; }
            set { SetItem(Indexes.HelmetDye, value); }
        }

        public virtual TItem ChestplateDye
        {
            get { return InternalItems[Indexes.ChestplateDye]; }
            set { SetItem(Indexes.ChestplateDye, value); }
        }

        public virtual TItem LeggingDye
        {
            get { return InternalItems[Indexes.LeggingDye]; }
            set { SetItem(Indexes.LeggingDye, value); }
        }

        public virtual TItem Pet
        {
            get { return InternalItems[Indexes.Pet]; }
            set { SetItem(Indexes.Pet, value); }
        }

        public virtual TItem LightPet
        {
            get { return InternalItems[Indexes.LightPet]; }
            set { SetItem(Indexes.LightPet, value); }
        }

        public virtual TItem Minecart
        {
            get { return InternalItems[Indexes.Minecart]; }
            set { SetItem(Indexes.Minecart, value); }
        }

        public virtual TItem Mount
        {
            get { return InternalItems[Indexes.Mount]; }
            set { SetItem(Indexes.Mount, value); }
        }

        public virtual TItem GrapplingHook
        {
            get { return InternalItems[Indexes.GrapplingHook]; }
            set { SetItem(Indexes.GrapplingHook, value); }
        }

        public virtual TItem PetDye
        {
            get { return InternalItems[Indexes.PetDye]; }
            set { SetItem(Indexes.PetDye, value); }
        }

        public virtual TItem LightPetDye
        {
            get { return InternalItems[Indexes.LightPetDye]; }
            set { SetItem(Indexes.LightPetDye, value); }
        }

        public virtual TItem MinecartDye
        {
            get { return InternalItems[Indexes.MinecartDye]; }
            set { SetItem(Indexes.MinecartDye, value); }
        }

        public virtual TItem MountDye
        {
            get { return InternalItems[Indexes.MountDye]; }
            set { SetItem(Indexes.MountDye, value); }
        }

        public virtual TItem GrapplingHookDye
        {
            get { return InternalItems[Indexes.GrapplingHookDye]; }
            set { SetItem(Indexes.GrapplingHookDye, value); }
        }

        public virtual TItem Trash
        {
            get { return InternalItems[Indexes.Trash]; }
            set { SetItem(Indexes.Trash, value); }
        }

        public TItem this[int i] => InternalItems[i];

        internal readonly TItem[] InternalItems = new TItem[Indexes.Max];

        IEnumerator IEnumerable.GetEnumerator()
            => All.GetEnumerator();

        public PlayerInventory() { }

        internal PlayerInventory(Player parent) : base(parent)
        {
            
        }

        public virtual void SetItem(int slot, TItem item)
        {
            ThrowIfOutOfBounds(slot);

            if (IsStub)
                InternalItems[slot] = item;
            else
                throw new NotSupportedException("Inventory values can only be set for the current player.");
        }

        protected void ThrowIfOutOfBounds(int slot)
        {
            if (slot > Indexes.Max || 0 > slot)
                throw new IndexOutOfRangeException(
                    $"The slot argument passed to SetItem is out of bounds: {slot}, Min: 0 Max: {Indexes.Max}");
        }

        public virtual void SendItemPacket(int slot)
        {
            throw new NotSupportedException("Inventory values can only be set for the current player.");
        }

        public IEnumerator GetEnumerator()
            => All.GetEnumerator();
    }

    public sealed class CurrentPlayerInventory : PlayerInventory
    {
        public override TItem Helmet
        {
            set { SetItem(Indexes.Helmet, value); }
        }

        public override TItem Chestplate
        {
            set { SetItem(Indexes.Chestplate, value); }
        }

        public override TItem Leggings
        {
            set { SetItem(Indexes.Leggings, value); }
        }

        public override TItem SocialHelmet
        {
            set { SetItem(Indexes.SocialHelmet, value); }
        }

        public override TItem SocialChestplate
        {
            set { SetItem(Indexes.SocialChestplate, value); }
        }

        public override TItem SocialLeggings
        {
            set { SetItem(Indexes.SocialLeggings, value); }
        }

        public override TItem HelmetDye
        {
            set { SetItem(Indexes.HelmetDye, value); }
        }

        public override TItem ChestplateDye
        {
            set { SetItem(Indexes.ChestplateDye, value); }
        }

        public override TItem LeggingDye
        {
            set { SetItem(Indexes.LeggingDye, value); }
        }

        public override TItem Pet
        {
            set { SetItem(Indexes.Pet, value); }
        }

        public override TItem LightPet
        {
            set { SetItem(Indexes.LightPet, value); }
        }

        public override TItem Minecart
        {
            set { SetItem(Indexes.Minecart, value); }
        }

        public override TItem Mount
        {
            set { SetItem(Indexes.Mount, value); }
        }

        public override TItem GrapplingHook
        {
            set { SetItem(Indexes.GrapplingHook, value); }
        }

        public override TItem PetDye
        {
            set { SetItem(Indexes.PetDye, value); }
        }

        public override TItem LightPetDye
        {
            set { SetItem(Indexes.LightPetDye, value); }
        }

        public override TItem MinecartDye
        {
            set { SetItem(Indexes.MinecartDye, value); }
        }

        public override TItem MountDye
        {
            set { SetItem(Indexes.MountDye, value); }
        }

        public override TItem GrapplingHookDye
        {
            set { SetItem(Indexes.GrapplingHookDye, value); }
        }

        public override TItem Trash
        {
            set { SetItem(Indexes.Trash, value); }
        }

        internal CurrentPlayerInventory(PlayerInventory inventory) : base(inventory.Parent)
        {
            inventory.InternalItems.CopyTo(InternalItems, 0);
        }

        // todo : default inventory?
        public override void SetItem(int slot, TItem item)
        {
            ThrowIfOutOfBounds(slot);
            InternalItems[slot] = item;
            SendItemPacket(slot);
        }

        public override void SendItemPacket(int slot)
        {
            ThrowIfOutOfBounds(slot);
            Parent.Client.Sender.Send(
                TPacket.Build(TPacketType.PlayerInventorySlotInfo,
                    new PacketPlayerInventorySlotInfo(Parent.PlayerId, (byte) slot, InternalItems[slot])));
            ;
        }
    }
}