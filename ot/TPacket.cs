﻿using System;

namespace OpenTerraria.ot
{
    public class TPacket
    {
        public const byte OpcodeSize = sizeof(byte);
        public const byte LenghtSize = sizeof(ushort);

        public const byte LengthIndex = 0;
        public const byte OpcodeIndex = LengthIndex + LenghtSize;
        public const byte PayloadIndex = OpcodeIndex + OpcodeSize;

        public const ushort EmptyPacketSize = OpcodeSize + LenghtSize;

        public TPacketType Type { get; }
        public ushort Length { get; }
        public byte[] Payload { get; }

        public byte[] Raw { get; }
        public int RawLength { get; }

        private TPacket(ushort length, TPacketType type, byte[] payload, byte[] raw, int rawLength)
        {
            Type = type;
            Length = length;
            Payload = payload;
            Raw = raw;
            RawLength = rawLength;
        } 

        // asume raw is 100% correct
        internal static TPacket Build(byte[] raw, int startIndex)
        {
            var packetLength = BitConverter.ToUInt16(raw, startIndex + LengthIndex);
            var payloadLength = packetLength - EmptyPacketSize;
            var payload = new byte[payloadLength];
            // size of payload does not include length + type, remove it.

            Buffer.BlockCopy(raw, startIndex + PayloadIndex, payload, 0, payloadLength);

            return new TPacket(packetLength,
                (TPacketType) raw[startIndex + OpcodeIndex],
                payload,
                raw,
                packetLength);
        }

        public static TPacket Build(TPacketType type, ushort payloadLength = 0, byte[] payload = null)
        {
            var packetLength = EmptyPacketSize + payloadLength;
            var bPacket = new byte[packetLength];

            // set length
            Buffer.BlockCopy(BitConverter.GetBytes(packetLength), 0, bPacket, LengthIndex, LenghtSize);
            // set opcode
            bPacket[OpcodeIndex] = (byte)type;
            // set payload
            if (payload != null)
                Buffer.BlockCopy(payload, 0, bPacket, PayloadIndex, payloadLength);

            return new TPacket((ushort)packetLength, type, payload, bPacket, packetLength);
        }

        public static TPacket Build(TPacketType type, IPacketPayloadWrapper wrapper)
        {
            var tuple = wrapper.BuildPayload();
            return Build(type, tuple.Length, tuple.Payload);
        }
    }
}