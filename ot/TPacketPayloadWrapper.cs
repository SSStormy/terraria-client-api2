﻿namespace OpenTerraria.ot
{
    public interface IPacketPayloadWrapper
    {
        PayloadLengthTuple BuildPayload();

        /// <summary>
        /// Reads data from the given payload into our fields/properties.
        /// </summary>
        void FromPayload(byte[] payload);
    }

    public abstract class PacketPayloadWrapper : IPacketPayloadWrapper
    {
        public PacketPayloadWrapper(TPacket packet)
        {
            FromPayload(packet.Payload);
        }

        public PacketPayloadWrapper()
        {
            
        }

        public PayloadLengthTuple BuildPayload()
            => PayloadIO.GetBytes(this);

        public void FromPayload(byte[] payload)
            => PayloadIO.WriteInto(payload, this);
    }
}
