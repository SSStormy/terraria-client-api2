﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using OpenTerraria.ot.Models;
using OpenTerraria.ot.Models.Ids;
using OpenTerraria.ot.Packets;

namespace OpenTerraria.ot
{
    public partial class TerrariaClient : IDisposable
    {
        // todo : put this into a config
        public const string TerrariaVersionString = "Terraria169";
        public PacketReceiver Receiver { get; }
        public PacketSender Sender { get; }

        internal Socket NetSocket { get; }

        public bool IsConnected => NetSocket.Connected;
        public bool IsLoggedIn { get; private set; }

        public CurrentPlayer Player { get; private set; }
        public TWorld World { get; private set; }
        public StatusBar Status { get; private set; }

        private readonly ManualResetEvent _hasDisconnectedEvent = new ManualResetEvent(true);
        private readonly ManualResetEvent _loginEvent = new ManualResetEvent(false);

        private bool _isDisposed;
        private CancellationTokenSource _keepaliveCancel;
        private string _password;

        // todo : send guid
        // todo : rename this project (OpenTerraria is already used)
        // todo : chest caching
        // todo : player caching
        // todo : sendsection caching
        // todo : npc caching
        // todo : projectile caching

        // todo : on login event
        // todo : on message received event?
        // todo : longterm: tests
        // todo : player connect, leave events

        public TerrariaClient(ProtocolType protocolType = ProtocolType.IP)
        {
            NetSocket = new Socket(SocketType.Stream, protocolType);
            Receiver = new PacketReceiver(this);
            Sender = new PacketSender(this);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_isDisposed)
            {
                if (isDisposing)
                {
                    NetSocket?.Dispose();
                    _hasDisconnectedEvent?.Dispose();
                    _keepaliveCancel?.Dispose();
                    _loginEvent?.Dispose();
                }
                _isDisposed = true;
            }
        }

        /// <summary>
        ///     Establishes a connection with the Terraria server
        /// </summary>
        public Task Connect(string ip, int port)
        {
            var connectResetEvent = new ManualResetEvent(false);

            return Task.Run(async () =>
            {
                await Disconnect().ConfigureAwait(false);

                NetSocket.BeginConnect(ip, port, r =>
                {
                    NetSocket.EndConnect(r);
                    Receiver.Start();
                    connectResetEvent.Set();
                }, null);

                connectResetEvent.WaitOne();
                connectResetEvent.Dispose();
            });
        }

        public Task Disconnect()
        {
            var disconnectResetEvent = new ManualResetEventSlim(false);

            return Task.Run(() =>
            {
                Receiver.Stop();
                Receiver.Reset();
                _hasDisconnectedEvent.Reset();

                if (IsConnected)
                {
                    _keepaliveCancel?.Cancel();
                    IsLoggedIn = false;
                    Player = null;
                    World = null;
                    Status = null;
                    _password = null;

                    // todo : REMINDER : reset ALL cached information on disconnect
                    NetSocket.Shutdown(SocketShutdown.Both);

                    NetSocket.BeginDisconnect(true, r =>
                    {
                        NetSocket.EndDisconnect(r);
                        disconnectResetEvent.Set();
                    }, null);
                }
                else
                    disconnectResetEvent.Set();

                disconnectResetEvent.Wait();
                _hasDisconnectedEvent.Set();
                disconnectResetEvent.Dispose();
            });
        }

        /// <summary>
        ///     Attempts to log in to the Terraria server.
        /// </summary>
        /// <param name="password">The optional password to provide to the server if it requires one.</param>
        public Task Login(short health = Models.Player.DefaultHp, short maxHealth = Models.Player.DefaultHp,
            short mana = Models.Player.DefaultMana, short maxMana = Models.Player.DefaultMana,
            PlayerBuffs buffs = null, PlayerInventory inventory = null,
            PlayerInfo info = null, string password = null)
        {
            return Task.Run(() =>
            {
                if (IsLoggedIn)
                    return; // todo : throw? maybe reconnect?

                _loginEvent.Reset();
                _password = password;

                Player = new CurrentPlayer(this, info, inventory, buffs)
                {
                    Health = health,
                    HealthMax = maxHealth,
                    Mana = mana,
                    ManaMax = maxHealth
                };

                Receiver.PacketReceived += InternalLoginPacketHandler;
                Receiver.PacketReceived += InternalPacketHandler;

                Sender.Send(TPacket.Build(TPacketType.ConnectRequest, PacketGeneric.Create(TerrariaVersionString)));
                _keepaliveCancel = new CancellationTokenSource();

                _loginEvent.WaitOne();
            });
        }

        private void InternalPacketHandler(object sender, PacketReceivedEventArgs e)
        {
            switch (e.Packet.Type)
            {
                case TPacketType.WorldInformation:
                    World.Update(new PacketWorldInfo(e.Packet));
                    World.InternalPlayers[Player.PlayerId] = Player;
                    break;
                case TPacketType.FatalError:
                    _loginEvent.Set();
                    var msg = new PacketGeneric<string>(e.Packet).Value;

                    if (msg == "Incorrect password.")
                        throw new InvalidPasswordException();

                    if (msg == "You are banned from this server." ||
                        msg == "Banned from server.")
                        throw new BannedFromServerException();

                    OnFatalError(new FatalErrorEventArgs(msg));
                    break;
                case TPacketType.Statusbar:
                    Status = new StatusBar(e.Packet);
                    OnStatsBarReceived(new StatusBarReceivedEventArgs(Status));
                    break;
                case TPacketType.UpdateItemDrop:
                case TPacketType.UpdateItemDrop2:
                    var pUpdateItem = new PacketUpdateItemDrop(e.Packet);
                    var item = World.InternalWorldItems[pUpdateItem.UniqueItemId];

                    if (pUpdateItem.ItemId == 0)
                    {
                        item.IsActive = false;
                        break;
                    }

                    // todo : item.newAndShiny?
                    // todo : do something with NoDelay
                    item.Item = new TItem((TItemId) pUpdateItem.ItemId, (TItemPrefix) pUpdateItem.Prefix,
                        pUpdateItem.Amount);
                    item.Position = pUpdateItem.Position;
                    item.Velocity = pUpdateItem.Velocity;
                    item.IsActive = true;

                    if (e.Packet.Type == TPacketType.UpdateItemDrop2)
                        item.Owner = Player.PlayerId;
                    break;
                case TPacketType.UpdateItemOwner:
                    var pUpdateOwner = new PacketGeneric<short, byte>(e.Packet);
                    World.InternalWorldItems[pUpdateOwner.Value1].Owner = pUpdateOwner.Value2;
                    break;
                case TPacketType.RemoveItemOwner:
                    var index = BitConverter.ToInt16(e.Packet.Payload, 0);
                    World.InternalWorldItems[index].Owner = TWorldItem.DefaultOwner;
                    Sender.Send(TPacket.Build(TPacketType.UpdateItemOwner,
                        PacketGeneric.Create(index, TWorldItem.DefaultOwner)));
                    break;
                case TPacketType.PlayerInfo:
                    var info = new PlayerInfo(e.Packet);

                    if (info.PlayerId == Player.PlayerId)
                        break;
                    var infoPlayer = World.InternalPlayers[info.PlayerId];
                    info.Parent = infoPlayer;
                    infoPlayer.LoadoutInfo = info;

                    break;
                case TPacketType.PlayerInventorySlotInfo:
                    var pSlotInfo = new PacketPlayerInventorySlotInfo(e.Packet);

                    if (pSlotInfo.PlayerId == Player.PlayerId)
                        break;

                    World.InternalPlayers[pSlotInfo.PlayerId].Inventory.InternalItems[pSlotInfo.Slot] =
                        new TItem((TItemId) pSlotInfo.ItemId, (TItemPrefix) pSlotInfo.PrefixId, pSlotInfo.Amount);
                    break;
                case TPacketType.SpawnPlayer:
                    var pSpawnPlayerId = e.Packet.Payload[0];
                    World.InternalPlayers[pSpawnPlayerId].IsDead = false;
                    // todo : on spawn event?
                    break;
                case TPacketType.UpdatePlayer:
                    var pUpdate = new PacketUpdatePlayer(e.Packet);

                    if (pUpdate.PlayerId == Player.PlayerId)
                        break;

                    World.InternalPlayers[pUpdate.PlayerId].Update(pUpdate);
                    break;
                case TPacketType.PlayerActive:
                    var pActive = new PacketGeneric<byte, bool>(e.Packet);
                    World.InternalPlayers[pActive.Value1].IsActive = pActive.Value2;
                    // todo : on available player event?
                    // todo : on player active state changed event?
                    break;
                // todo : handle Time [18] packet
                // todo : handle chat msg [25] packet
                // todo : player damage [26]
                // todo : heal effect [35] ???
                // todo : parse set zone [36]
                // todo : parse player mana [42] ignore if pid is us
                // todo : parse player health  [17] ignore if pid is us
                // todo : ReplenishMana [43]
                // todo : add player buff [55]
                // todo : heal other player [66]
                // todo : set player stealth [84]
                case TPacketType.TogglePvp:
                    var pTogglePvp = new PacketGeneric<byte, bool>(e.Packet);
                    World.InternalPlayers[pTogglePvp.Value1].IsPvp = pTogglePvp.Value2;
                    break;
                case TPacketType.PlayerTeam:
                    var pTeam = new PacketGeneric<byte, byte>(e.Packet);
                    World.InternalPlayers[pTeam.Value1].Team = (Player.TeamType) pTeam.Value2;
                    break;
                case TPacketType.UpdatePlayerBuff:
                    var pUpdateBuff = new PacketUpdatePlayerBuff(e.Packet);

                    if (pUpdateBuff.PlayerId == Player.PlayerId)
                        break;

                    World.InternalPlayers[pUpdateBuff.PlayerId].Buffs.Update(pUpdateBuff);
                    break;
                case TPacketType.PlayerMana:
                    var pMana = new PacketGeneric<byte, short, short>(e.Packet);

                    if (pMana.Value1 == Player.PlayerId)
                        break;
                    Player manaPlayer = World.InternalPlayers[pMana.Value1];
                    manaPlayer.Mana = pMana.Value2;
                    manaPlayer.ManaMax = pMana.Value3;
                    break;
                case TPacketType.PlayerHealth:
                    var pHealth = new PacketGeneric<byte, short, short>(e.Packet);

                    if (pHealth.Value1 == Player.PlayerId)
                        break;
                    Player healthPlayer = World.InternalPlayers[pHealth.Value1];
                    healthPlayer.Health = pHealth.Value2;
                    healthPlayer.HealthMax = pHealth.Value3;
                    break;
            }
        }

        private void InternalLoginPacketHandler(object sender, PacketReceivedEventArgs e)
        {
            switch (e.Packet.Type)
            {
                case TPacketType.ContinueConnecting:
                    Player.PlayerId = e.Packet.Payload[0];
                    Sender.Send(TPacket.Build(TPacketType.PlayerInfo, Player.LoadoutInfo));
                    Sender.Send(TPacket.Build(TPacketType.PlayerHealth,
                        PacketGeneric.Create(Player.PlayerId, Player.Health, Player.HealthMax)));
                    Sender.Send(TPacket.Build(TPacketType.PlayerMana,
                        PacketGeneric.Create(Player.PlayerId, Player.Mana, Player.ManaMax)));
                    Sender.Send(TPacket.Build(TPacketType.UpdatePlayerBuff, new PacketUpdatePlayerBuff(Player.Buffs)));

                    for (var i = 0; i < PlayerInventory.Indexes.Max; i++)
                        Player.Inventory.SendItemPacket(i);

                    Sender.Send(TPacket.Build(TPacketType.RequestWorldInformation));
                    break;
                case TPacketType.RequestPassword:
                    if (string.IsNullOrEmpty(_password))
                        throw new NoPasswordProvidedException();

                    Sender.Send(TPacket.Build(TPacketType.SendPassword, PacketGeneric.Create(_password)));
                    break;
                case TPacketType.WorldInformation:
                    World = new TWorld(this, new PacketWorldInfo(e.Packet));
                    Sender.Send(TPacket.Build(TPacketType.SyncSection, PacketGeneric.Create(World.SpawnX, World.SpawnY)));
                    break;
                case TPacketType.CompleteConnectionAndSpawn:
                    Sender.Send(TPacket.Build(TPacketType.SpawnPlayer,
                        PacketGeneric.Create(Player.PlayerId, World.SpawnX, World.SpawnY)));
                    _loginEvent.Set();
                    IsLoggedIn = true;
                    Receiver.PacketReceived -= InternalLoginPacketHandler;
                    StartKeepaliveStream();
                    break;
            }
        }

        private void StartKeepaliveStream()
        {
            try
            {
                // todo : option to opt out of the keepalive stream
                Task.Run(async () =>
                {
                    while (true)
                    {
                        if (_keepaliveCancel.IsCancellationRequested)
                            _keepaliveCancel.Token.ThrowIfCancellationRequested();

                        // fuck the police
                        Sender.Send(TPacket.Build(TPacketType.NullNeverSent));

                        // todo : option to set the wait time somewhere?
                        await Task.Delay(5000, _keepaliveCancel.Token).ConfigureAwait(false);
                    }
                }, _keepaliveCancel.Token);
            }
            catch (TaskCanceledException)
            {
                // expected
            }
        }
    }

    public class NoPasswordProvidedException : Exception
    {
    }

    public class InvalidPasswordException : Exception
    {
    }

    public class BannedFromServerException : Exception
    {
    }
}