﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace OpenTerraria.ot
{
    public class PayloadLengthTuple
    {
        public ushort Length { get; }
        public byte[] Payload { get; }

        internal PayloadLengthTuple(ushort length, byte[] payload)
        {
            Length = length;
            Payload = payload;
        }
    }

    /// <summary>
    ///     Controls the building bytes from payload wrappers and writing bytes into payload wrappers
    /// </summary>
    public static class PayloadIO
    {
        [AttributeUsage(AttributeTargets.Property)]
        public class IgnoreMemberAttribute : Attribute
        {
        }

        /// <summary>
        ///     A dict of cached property info's for the given key type.
        /// </summary>
        private static readonly ConcurrentDictionary<Type, List<PropertyInfo>> typeIO
            = new ConcurrentDictionary<Type, List<PropertyInfo>>();

        private static List<PropertyInfo> GetPropInfo(Type type)
        {
            if (!typeIO.ContainsKey(type))
                typeIO.TryAdd(type, BuildPropInfo(type));

            return typeIO[type];
        }

        /// <summary>
        ///     Creates a list of property infos for io opertaions for the given type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static List<PropertyInfo> BuildPropInfo(Type type)
        {
            // find all properties that can be converted
            // find how to write/read them

            var retval = new List<PropertyInfo>();

            foreach (var prop in type.GetRuntimeProperties())
            {
                if (prop.GetCustomAttribute<IgnoreMemberAttribute>() == null)
                {
                    // todo : sequencing control
                    retval.Add(prop);
                }
            }

            return retval;
        }

        // todo : support TPacketPayloadWrapper nesting
        public static PayloadLengthTuple GetBytes(IPacketPayloadWrapper wrapper)
        {
            // todo : integrate a stopwatch
            using (var pStream = new MemoryStream())
            using (var writer = new BinaryWriter(pStream))
            {
                foreach (var prop in GetPropInfo(wrapper.GetType()))
                {
                    var data = prop.GetValue(wrapper);
                    if (data == null)
                    {
                        // todo : global logger
                        Debug.Assert(false);
                    }

                    if (!GenericTypeConverter.CanConvert(data.GetType()))
                    {
                        Debug.Assert(false);
                    }

                    GenericTypeConverter.Write(writer, data);
                }
                return new PayloadLengthTuple(Convert.ToUInt16(pStream.Length), pStream.ToArray());
            }
        }

        public static void WriteInto(byte[] payload, IPacketPayloadWrapper wrapper)
        {
            // todo : provide lenght, check if what we will write to will be more then payload or less then payload.

            using (var pStream = new MemoryStream(payload))
            using (var reader = new BinaryReader(pStream))
                foreach (var prop in GetPropInfo(wrapper.GetType()))
                {
                    if (!GenericTypeConverter.CanConvert(prop.PropertyType))
                    {
                        Console.WriteLine($"Data of {prop.Name} in {wrapper.GetType().Name} cannot be converted!");
                        continue;
                    }

                    prop.SetValue(wrapper, GenericTypeConverter.Read(reader, prop.PropertyType));
                }
        }
    }
}