﻿using System;
using System.Threading.Tasks;
using OpenTerraria.ot;
using OpenTerraria.ot.Models;
using OpenTerraria.ot.Models.Ids;
using OpenTerraria.ot.Packets;
// ReSharper disable ConsiderUsingConfigureAwait

namespace OpenTerraria
{
    internal class Program
    {
        // server vulnerability: 
        // server eches killmes even if we haven't logged in.
        private static void Main()
        {
            new Program().Start().GetAwaiter().GetResult();
            Console.ReadLine();
        }

        public async Task Start()
        {
            var client = new TerrariaClient();

            await client.Connect("213.226.180.166", 7777);
            var app = PlayerInfo.Default;
            var inv = new PlayerInventory();
            var buff = new PlayerBuffs();
            inv.Helmet = TItemId.BeetleHelmet;
            app.Name = "dad";
            // todo : this didn't fuckin work
            buff.AddBuff(new TBuff(TBuffId.Invisibility, 5));

            Events(client);
            await client.Login(info: app, inventory: inv, buffs: buff, password: "no");

            //await Task.Delay(1500);

            //await client.Disconnect();
            //await Task.Delay(100);
            //await client.Connect("213.226.180.166", 7777);
            //app.Name = "Dad";
            //await client.Login(appearance: app, inventory: inv, buffs: buff);
            //Events(client);
        }

        private void Events(TerrariaClient client)
        {
            client.Receiver.PacketReceived += (s, e) =>
            {
                if (e.Packet.Type == TPacketType.SetNpcKillCount ||
                e.Packet.Type == TPacketType.PlayerActive ||
                e.Packet.Type == TPacketType.PlayerInventorySlotInfo) return;
                Console.WriteLine($"{e.Packet.Type}");
            };
            client.StatusBarReceived += (s, e) => Console.WriteLine($"Status[Id: {e.Status.Count} Msg:{e.Status.Message}]");
            client.FatalError += (s, e) => Console.WriteLine($"FATAL: {e.Message}");
        }
    }
}